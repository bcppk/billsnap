import { fetch } from 'whatwg-fetch';
import decode from "jwt-decode";
import config from "../config";

export default class Auth {
    constructor() {
        this.fetch = this.fetch.bind(this);
        this.login = this.login.bind(this);
        this.getProfile = this.getProfile.bind(this);
    }

    login(username, password) {
        return this.fetch(`${config.ApiUrl}/staff/login`, {
            method: 'POST',
            body: JSON.stringify({
                "LoginForm": {
                    "username": username,
                    "password": password
                }
            })
        }).then(res => {
            this.setToken(res.data.access_token);
            return Promise.resolve(res);
        })
    }

  customerDetails(name, email, mobile) {
    return this.fetch(`${config.ApiUrl}/user/registration`, {
      method: 'POST',
      body: JSON.stringify({
        "campaign_id": 1,
        "customername": name,
        "email": email,
        "mobile": mobile
      })
    }).then(res => {
      return Promise.resolve(res);
    })
  }

  validateOtp(mobile, otp) {
    return this.fetch(`${config.ApiUrl}/user/validateotp`, {
      method: 'POST',
      body: JSON.stringify({
        "campaign_id": 1,
        "mobile": mobile,
        "otp": otp
      })
    }).then(res => {
      this.setToken(res.data.access_token);
      return Promise.resolve(res);
    })
  }

  submitBill(bill) {
    return this.fetch(`${config.ApiUrl}/user/uploadbill`, {
      method: 'POST',
      body: JSON.stringify({
        "campaign_id": 1,
        "bill": bill
      })
    }).then(res => {
      return Promise.resolve(res);
    })
  }

    loggedIn() {
        const token = this.getToken();
        return !!token && !this.isTokenExpired(token);
    }

    isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) {
                return true;
            }
            else
                return false;
        }
        catch (err) {
            return false;
        }
    }

    setToken(idToken) {
        localStorage.setItem(config.TokenName, idToken);
    }

    getToken() {
        return localStorage.getItem(config.TokenName);
    }

    logout() {
        localStorage.removeItem(config.TokenName);
    }

    getProfile() {
        return decode(this.getToken());
    }

    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken();
        }

        return fetch(url, {
            headers,
            ...options
        })
                .then(this._checkStatus)
                .then(response => response.json())
    }

    _checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response;
        } else {
            let error = new Error("Username or password is incorrect.");
            throw error;
        }
    }
}