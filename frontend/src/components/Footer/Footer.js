import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
  render() {
    return(
        <footer className="footer">
          <div className="container">
            <p className="pull-left">&copy; BigCity Promotions 2018,</p>
            <p className="pull-left">All Rights Reserved</p>
          </div>
        </footer>
    );
  }
}

export default Footer;