import React, { Component } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Auth from '../../utils/Auth';
import './Bill.css';
import Header from '../Header';
import Footer from '../Footer';

import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';

class Bill extends Component {
  constructor(props) {
    super(props);

    this._auth = new Auth();

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCrop = this.handleCrop.bind(this);
    this.handleTakePic = this.handleTakePic.bind(this);

    this.state = {
      initCam: true,
      capturedImg: null,
      croppedImage: null,
      crop: {
        x: 10,
        y: 10,
        width: 80,
        height: 80,
      },
      errorMsg: '',
      successMsg: '',
      isSubmitted: false,
      showSubmit: false,
      src: '',
      err: null,
      imgPreview: false
    };
  };

  componentWillUnmount() {
    this.setState({
      errorMsg: '',
      successMsg: ''
    });
  }

  onTakePhoto (dataUri) {
    // Do stuff with the dataUri photo...
    this.turnOffCamera();

    setTimeout(() => {
      this.setState({
        initCam: false,
        capturedImg: dataUri,
        croppedImage: null
      });
    }, 1000)
  }

  turnOffCamera() {
    let stream = document.querySelector('video').srcObject;
    let tracks = stream.getTracks();

    tracks.forEach(function(track) {
      track.stop();
    });

    document.querySelector('video').srcObject = null;
  }

  onCameraError (error) {
    console.error('onCameraError', error);
  }

  onCameraStart (stream) {
    console.log('onCameraStart');
  }

  onCameraStop () {
    console.log('onCameraStop');
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      isSubmitted: true
    });

    this._auth.submitBill(this.state.croppedImage)
        .then(res => {
          //alert(res.data.message);
          this.setState({successMsg: res.data.message});
          setTimeout(() => {
            this.props.history.replace('/');
          }, 5000);
        })
        .catch(err => {
          this.setState({
            errorMsg: 'Server error! Please try again.',
            isSubmitted: false
          });
        })
  }

  onCropComplete = (crop, pixelCrop) => {
    console.log('onCropComplete', pixelCrop);
  };

  onCropChange = crop => {
    this.setState({ crop })
  };

  handleCrop = () => {
    let { crop } = this.state;

    const croppedImg = this.getCroppedImg(this.refImageCrop, crop);
    this.setState({ croppedImage: croppedImg, showSubmit: true, imgPreview: true });
  };

  getCroppedImg = (srcImage, pixelCrop) => {
    let img = new Image();
    img.src = this.state.capturedImg;
    const targetX = srcImage.width * pixelCrop.x / 100;
    const targetY = srcImage.height * pixelCrop.y / 100;
    const targetWidth = srcImage.width * pixelCrop.width / 100;
    const targetHeight = srcImage.height * pixelCrop.height / 100;

    const canvas = document.createElement('canvas');
    canvas.width = targetWidth;
    canvas.height = targetHeight;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
        img,
        targetX,
        targetY,
        targetWidth,
        targetHeight,
        0,
        0,
        targetWidth,
        targetHeight
    );

    return canvas.toDataURL('image/png');
  };

  handleTakePic = () => {
    this.setState({imgPreview: false, initCam: true});
  };

  render() {
    const WebCam = () => {
      return(
          <Camera
              onTakePhoto = { (dataUri) => { this.onTakePhoto(dataUri); } }
              onCameraError = { (error) => { this.onCameraError(error); } }
              idealFacingMode = {FACING_MODES.ENVIRONMENT}
              idealResolution = {{width: 640, height: 480}}
              imageType = {IMAGE_TYPES.PNG}
              imageCompression = {0.97}
              isMaxResolution = {false}
              isImageMirror = {false}
              isDisplayStartCameraError = {true}
              sizeFactor = {1}
              onCameraStart = { (stream) => { this.onCameraStart(stream); } }
              onCameraStop = { () => { this.onCameraStop(); } }
          />
      );
    };

    if (this.state.initCam) {
      return (
          <WebCam />
      );
    } else {
      return (
          <div>
            <div className="wrap">
              <Header/>
              <div className="container">
                <div className="site-signup">
                  {!this.state.imgPreview &&
                    <div className="col-md-12 well bs-component">
                      <p>Please crop the bill and submit:</p>
                      <div className="form-group">
                        <ReactCrop
                            src={this.state.capturedImg}
                            crop={this.state.crop}
                            keepSelection={true}
                            onComplete={this.onCropComplete}
                            onChange={this.onCropChange}
                        />
                      </div>
                      <div id="form-error" className="form-group">{this.state.errorMsg}</div>
                      <div className="form-group">
                        <button type="button" className="btn btn-default pull-right" onClick={this.handleCrop}>Crop</button>
                      </div>
                    </div>
                  }

                  {this.state.imgPreview &&
                    <div className="col-md-12 well bs-component">
                      <p>Are you able to read this image?</p>
                      <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                          <img id="finalImg" src={this.state.croppedImage} alt=""/>
                        </div>
                        <p className="green">{this.state.successMsg}</p>
                        <div className="form-group">
                          {this.state.showSubmit ?
                              <button type="submit" className="btn btn-primary" disabled={this.state.isSubmitted}>{this.state.isSubmitted ?
                                  <i className="fa fa-spinner fa-spin"></i> : null}Yes, submit</button>
                              : null
                          }
                          <button type="button" className="btn btn-default" onClick={this.handleTakePic} style={{marginLeft:'10px'}}>No, take another pic</button>
                        </div>
                      </form>
                    </div>
                  }

                  <img src={this.state.capturedImg} style={{display: "none"}} ref={(img) => {
                    this.refImageCrop = img
                  }} alt=""/>
                </div>
              </div>
            </div>
            <Footer />
          </div>
      );
    }
  }
}

export default Bill;