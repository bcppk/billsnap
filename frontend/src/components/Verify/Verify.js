import React, { Component } from 'react';
import Auth from '../../utils/Auth';
import './Verify.css';
import Header from '../Header';
import Footer from '../Footer';

class Verify extends Component {
  constructor(props) {
    super(props);

    this._auth = new Auth();

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      errorMsg: '',
      usrmob: null,
      isSubmitted: false,
      errors: {}
    };
  }

  componentWillMount() {
    this.setState({usrmob: localStorage.getItem('usrmob')});
  }

  handleValidation() {
    let errors = {};
    let formIsValid = true;

    if(!this.state.otp) {
      formIsValid = false;
      errors["otp"] = "Please enter OTP.";
    }

    this.setState({errors: errors});
    return formIsValid;
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    if(this.handleValidation()) {
      this.setState({
        isSubmitted: true
      });
      this._auth.validateOtp(this.state.usrmob, this.state.otp)
          .then(res => {
            this.props.history.replace('/camera');
          })
          .catch(err => {
            this.setState({
              errorMsg: 'Invalid OTP!',
              isSubmitted: false
            });
          })
    }
  }

  render() {
    return (
        <div>
          <div className="wrap">
            <Header/>
            <div className="container">
              <div className="site-signup">
                <h1>Verify Mobile Number</h1>
                <div className="col-lg-5 well bs-component">
                  <form onSubmit={this.handleSubmit}>
                    <div className="form-group required">
                      <label className="control-label" htmlFor="otp">OTP</label>
                      <input type="text" id="otp" className="form-control" name="otp" aria-required="true" onChange={this.handleChange} />
                      <span className="error-text">{this.state.errors["otp"]}</span>
                    </div>
                    <div id="form-error" className="error-text">{this.state.errorMsg}</div>
                    <div className="form-group">
                      {this.state.isSubmitted ?
                          (<button type="submit" className="btn btn-primary" disabled={this.state.isSubmitted}><i className="fa fa-spinner fa-spin"></i>Submit</button>) :
                          (<button type="submit" className="btn btn-primary">Submit</button>)}
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <Footer />
        </div>
    );
  }
}

export default Verify;