import React, { Component } from 'react';
import Auth from '../../utils/Auth';
import './Homepage.css';
import Header from '../Header';
import Footer from '../Footer';

class Homepage extends Component {
  constructor(props) {
    super(props);

    this._auth = new Auth();

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      errors: {},
      errorMsg: '',
      isSubmitted: false
    };
  }

  handleValidation() {
    let errors = {};
    let formIsValid = true;

    if(!this.state.customername) {
      formIsValid = false;
      errors["customername"] = "Please enter your name.";
    }

    if(!this.state.email) {
      formIsValid = false;
      errors["email"] = "Please enter your email.";
    }

    if(typeof this.state.email !== "undefined") {
      let lastAtPos = this.state.email.lastIndexOf('@');
      let lastDotPos = this.state.email.lastIndexOf('.');

      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.email.indexOf('@@') == -1 && lastDotPos > 2 && (this.state.email.length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["email"] = "Please enter valid email ID.";
      }
    }

    if(!this.state.mobile) {
      formIsValid = false;
      errors["mobile"] = "Please enter 10 digit mobile number.";
    }

    if(typeof this.state.mobile !== "undefined") {
      if(!this.state.mobile.match(/^[0-9]+$/)) {
        formIsValid = false;
        errors["mobile"] = "Please enter valid mobile number";
      }
    }

    if(typeof this.state.mobile !== "undefined") {
      if(this.state.mobile.length != 10) {
        formIsValid = false;
        errors["mobile"] = "Please enter 10 digit mobile number.";
      }
    }

    this.setState({errors: errors});
    return formIsValid;
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    if(this.handleValidation()) {
      this.setState({
        isSubmitted: true
      });

      this._auth.customerDetails(this.state.customername, this.state.email, this.state.mobile)
          .then(res => {
            localStorage.setItem('usrmob', res.data.mobile);
            this.props.history.replace('/verify');
          })
          .catch(err => {
            this.setState({
              errorMsg: err.message,
              isSubmitted: false
            });
          })
    }
  }

  render() {
    return (
        <div>
          <div className="wrap">
            <Header/>
            <div className="container">
              <div className="site-signup">
                <h1>Registration</h1>
                <div className="col-lg-5 well bs-component">
                  <p>Please fill out the following fields to claim your reward:</p>
                  <form onSubmit={this.handleSubmit}>
                    <div className="form-group required">
                      <label className="control-label" htmlFor="customername">Customer Name</label>
                      <input type="text" id="customername" className="form-control" name="customername" aria-required="true" onChange={this.handleChange} />
                      <span className="error-text">{this.state.errors["customername"]}</span>
                    </div>
                    <div className="form-group required">
                      <label className="control-label" htmlFor="email">Email</label>
                      <input type="text" id="email" className="form-control" name="email" aria-required="true" onChange={this.handleChange} />
                      <span className="error-text">{this.state.errors["email"]}</span>
                    </div>
                    <div className="form-group required">
                      <label className="control-label" htmlFor="mobile">Mobile</label>
                      <input type="text" id="mobile" className="form-control" name="mobile" aria-required="true" onChange={this.handleChange} />
                      <span className="error-text">{this.state.errors["mobile"]}</span>
                    </div>
                    <div className="form-group required">
                      <div className="checkbox">
                        <label>
                          <input type="checkbox" id="agreeterms" name="agreeterms" value="1" className="agreeterms" onChange={this.handleChange} />
                          I agree to the <a id="terms-conditions">terms and conditions</a>
                        </label>
                      </div>
                    </div>
                    <div id="form-error" className="form-group">{this.state.errorMsg}</div>
                    <div className="form-group">
                      {this.state.isSubmitted ?
                          (<button type="submit" className="btn btn-primary" disabled={this.state.isSubmitted}><i className="fa fa-spinner fa-spin"></i>Submit</button>) :
                          (<button type="submit" className="btn btn-primary">Submit</button>)}
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <Footer />
        </div>
    );
  }
}

export default Homepage;