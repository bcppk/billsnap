<?php
    namespace app\modules\v1\controllers;

    use app\filters\auth\HttpBearerAuth;
    use app\models\CustomerData;
    use Yii;
    use yii\helpers\Json;
    use yii\data\ActiveDataProvider;
    use yii\filters\AccessControl;
    use yii\filters\auth\CompositeAuth;
    use yii\helpers\Url;
    use yii\rbac\Permission;
    use yii\rest\ActiveController;

    use yii\web\HttpException;
    use yii\web\NotFoundHttpException;
    use yii\web\ServerErrorHttpException;

    use app\models\User;
    use app\models\LoginForm;
    use app\models\Campaign;

    class StaffController extends ActiveController
    {
        public $modelClass = 'app\models\User';

        public function __construct($id, $module, $config = [])
        {
            parent::__construct($id, $module, $config);

        }

        public function actions()
        {
            return [];
        }

        public function behaviors()
        {
            $behaviors = parent::behaviors();

            $behaviors['authenticator'] = [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],

            ];

            $behaviors['verbs'] = [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'index'  => ['get'],
                    'view'   => ['get'],
                    'create' => ['post'],
                    'update' => ['put'],
                    'delete' => ['delete'],
                    'login'  => ['post'],
                    'getPermissions'    =>  ['get'],
                    'campaigns' => ['get'],
                    'campaign' =>['get'],
                    'createcampaign'=>['post'],
                    'updatecampaigns'=>['put'],
                    'users' => ['get', 'post'],
                    'user'=>['get'],
                    'createuser'=>['post'],
                    'updateUser' => ['put'],
                    'tldashboard' => ['get'],
                    'bulkassignlist' =>['get'],
                    'bulkassigntoagent' => ['post'],
                    'agentlist'         =>['get'],
                    'reassignlist' => ['get'],
                    'reassigntoagent' => ['post'],
                    'agentcampaignlist' => ['get'],
                    'agentdatalist' => ['get'],
                    'getcustomerdata' => ['get'],
                    'agentdataverify' => ['post'],
                    'getassignedlist' =>['get']
                ],
            ];

            // remove authentication filter
            $auth = $behaviors['authenticator'];
            unset($behaviors['authenticator']);

            // add CORS filter
            $behaviors['corsFilter'] = [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ];

            // re-add authentication filter
            $behaviors['authenticator'] = $auth;
            // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
            $behaviors['authenticator']['except'] = ['options', 'login'];


	        // setup access
	        $behaviors['access'] = [
		        'class' => AccessControl::className(),
		        'only' => ['getPermissions','campaigns' , 'campaign','createcampaign','updatecampaigns','users','user','createuser','updateUser','tldashboard','bulkassignlist','bulkassigntoagent','agentlist','reassignlist','reassigntoagent','agentcampaignlist','agentdatalist','getcustomerdata','agentdataverify','getassignedlist'], //only be applied to
		        'rules' => [
			        [
				        'allow' => true,
				        'actions' => ['getPermissions','campaigns' , 'campaign','createcampaign','updatecampaigns'],
				        'roles' => ['admin'],
			        ],
                    [
                        'allow' => true,
                        'actions' => ['getPermissions','campaigns' , 'campaign','createcampaign','updatecampaigns','users','user','createuser','updateUser'],
                        'roles' => ['superadmin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['campaigns','tldashboard','bulkassignlist','bulkassigntoagent','agentlist','reassignlist','reassigntoagent','getassignedlist'],
                        'roles' => ['staff'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['agentcampaignlist','agentdatalist','getcustomerdata','agentdataverify'],
                        'roles' => ['user'],
                    ],
		        ],
	        ];

            return $behaviors;
        }

	    /**
	     * Return list of staff members
	     *
	     * @return ActiveDataProvider
	     */
        public function actionIndex(){
            return new ActiveDataProvider([
                'query' =>  User::find()->where([
	                '!=', 'status', -1
                ])->andWhere([
	                'in', 'role', [User::ROLE_STAFF, User::ROLE_ADMIN]
                ])
            ]);
        }

	    /**
	     * Return requested staff member information
	     *
	     * Request: /v1/staff/2
	     *
	     * Sample Response:
	     * {
		 *   "success": true,
		 *   "status": 200,
		 *   "data": {
		 *	        "id": 2,
		 *		    "username": "staff",
		 *		    "email": "staff@staff.com",
		 *		    "unconfirmed_email": "lygagohur@hotmail.com",
		 *		    "role": 50,
		 *		    "role_label": "Staff",
		 *		    "last_login_at": "2017-05-20 18:58:40",
		 *		    "last_login_ip": "127.0.0.1",
		 *		    "confirmed_at": "2017-05-15 09:20:53",
		 *		    "blocked_at": null,
		 *		    "status": 10,
		 *		    "status_label": "Active",
		 *		    "created_at": "2017-05-15 09:19:02",
		 *		    "updated_at": "2017-05-21 23:31:32"
		 *	    }
		 *   }
	     *
	     * @param $id
	     *
	     * @return array|null|\yii\db\ActiveRecord
	     * @throws NotFoundHttpException
	     */
        public function actionView($id){
            $staff = User::find()->where([
	            'id'    =>  $id
            ])->andWhere([
	            '!=', 'status', -1
            ])->andWhere([
	            'in', 'role', [User::ROLE_STAFF, User::ROLE_ADMIN]
            ])->one();
            if($staff){
                return $staff;
            } else {
                throw new NotFoundHttpException("Object not found: $id");
            }
        }

	    /**
	     * Create new staff member from backend dashboard
	     *
	     * Request: POST /v1/staff/1
	     *
	     * @return User
	     * @throws HttpException
	     */
        public function actionCreate(){
            $model = new User();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(201);
                $id = implode(',', array_values($model->getPrimaryKey(true)));
                $response->getHeaders()->set('Location', Url::toRoute([$id], true));
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

	    /**
	     * Update staff member information from backend dashboard
	     *
	     * Request: PUT /v1/staff/1
	     *  {
		 *  	"id": 20,
		 *  	"username": "testuser",
		 *  	"email": "test2@test.com",
		 *  	"unconfirmed_email": "test2@test.com",
	     *  	"password": "{password}",
		 *  	"role": 50,
		 *  	"role_label": "Staff",
		 *  	"last_login_at": null,
		 *  	"last_login_ip": null,
		 *  	"confirmed_at": null,
		 *  	"blocked_at": null,
		 *  	"status": 10,
		 *  	"status_label": "Active",
		 *  	"created_at": "2017-05-27 17:30:12",
		 *  	"updated_at": "2017-05-27 17:30:12",
		 *  	"permissions": [
		 *  		{
		 *  			"name": "manageSettings",
		 *  			"description": "Manage settings",
		 *  			"checked": false
		 *  		},
		 *  		{
		 *  			"name": "manageStaffs",
		 *  			"description": "Manage staffs",
		 *  			"checked": false
		 *  		},
		 *  		{
		 *  			"name": "manageUsers",
		 *  			"description": "Manage users",
		 *  			"checked": true
		 *  		}
		 *  	]
		 *  }
	     *
	     *
	     * @param $id
	     *
	     * @return array|null|\yii\db\ActiveRecord
	     * @throws HttpException
	     */
        public function actionUpdate($id) {
            $model = $this->actionView($id);

            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

	    /**
	     * Delete requested staff member from backend dashboard
	     *
	     * Request: DELETE /v1/staff/1
	     *
	     * @param $id
	     *
	     * @return string
	     * @throws ServerErrorHttpException
	     */
        public function actionDelete($id) {
            $model = $this->actionView($id);

            $model->status = User::STATUS_DELETED;

            if ($model->save(false) === false) {
                throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
            }

            $response = \Yii::$app->getResponse();
            $response->setStatusCode(204);
            return "ok";
        }

	    /**
	     * Handle the login process for staff members for backend dashboard
	     *
	     * Request: POST /v1/staff/login
	     *
	     *
	     * @return array
	     * @throws HttpException
	     */
        public function actionLogin(){
            $model = new LoginForm();

	        $model->roles = [
	            User::ROLE_ADMIN,
	            User::ROLE_STAFF,
                User::ROLE_SUPERADMIN,
                User::ROLE_USER,
	        ];
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $user = $model->getUser();
                $user->generateAccessTokenAfterUpdatingClientInfo(true);

                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
                $id = implode(',', array_values($user->getPrimaryKey(true)));
                $user_role = User::findIdentity($id);
                $responseData = [
                    'id'    =>  $id,
                    'access_token' => $user->access_token,
                    'role'=>$user_role->role
                ];

                return $responseData;
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }
        }


	    /**
	     * Return list of available permissions for the staff.  The function will be called when staff form is loaded in backend.
	     *
	     * Request: GET /v1/staff/get-permissions
	     *
	     * Sample Response:
	     * {
		 *		"success": true,
		 *		"status": 200,
		 *		"data": {
		 *			"manageSettings": {
		 *				"name": "manageSettings",
		 *				"description": "Manage settings",
		 *				"checked": false
		 *			},
		 *			"manageStaffs": {
		 *				"name": "manageStaffs",
		 *				"description": "Manage staffs",
	     *				"checked": false
		 *			}
		 *		}
		 *	}
	     */
	    public function actionGetPermissions(){
		    $authManager = Yii::$app->authManager;

		    /** @var Permission[] $permissions */
		    $permissions = $authManager->getPermissions();

		    /** @var array $tmpPermissions to store list of available permissions */
		    $tmpPermissions = [];

		    /**
		     * @var string $permissionKey
		     * @var Permission $permission
		     */
		    foreach($permissions as $permissionKey => $permission) {
			    $tmpPermissions[] = [
		            'name'          =>  $permission->name,
		            'description'   =>  $permission->description,
		            'checked'       =>  false,
		        ];
		    }

		    return $tmpPermissions;
	    }

        public function actionOptions($id = null) {
            return "ok";
        }

        public function actionCampaigns() {
            $user                  = User::findIdentity(\Yii::$app->user->getId());
            if(isset($user->campaign_id) && !empty($user->campaign_id))
            {
                $campaignid            = explode(',', $user->campaign_id);
                $campaigns             = Campaign::find()->where(['IN','id',$campaignid])->all();
            }
            else{
                $campaigns             = Campaign::find()->all();
            }
            $responseData = [
                'campaigns'  => $campaigns,
                'user_role'  => $user->role
            ];

            return $responseData;
        }

        public function actionCampaign($id) {
            $user         = User::findIdentity(\Yii::$app->user->getId());
            $campaigns    = Campaign::find()->where(['id'=>$id])->one();
            $responseData = [
                'campaigns'  => $campaigns,
                'user_role'  => $user->role,
            ];
            return $responseData;
        }

        public function actionCreatecampaign(){
            $model = new Campaign();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->status          = Campaign::STATUS_ACTIVE;
            $model->created_date    = date('Y-m-d H:i:s');
            $model->updated_date    = date('Y-m-d H:i:s');
            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionUpdatecampaigns($id) {
            $model = Campaign::findOne($id);
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->updated_date = date('Y-m-d H:i:s');
            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionUsers($action) {
            if($action=='index'){
                $users = User::find()->all();
                return $users;
            }
            else{
                $campaigns  = Campaign::find()->where(['status'=>Campaign::STATUS_ACTIVE])->orderBy(['id'=>SORT_ASC])->all();
                return $campaigns;
            }
        }

        public function actionUser($id){
            $user = User::findOne($id);
            $campaigns  = Campaign::find()->where(['status'=>Campaign::STATUS_ACTIVE])->orderBy(['id'=>SORT_ASC])->all();
            if($user){
                $responseData = [
                    'user'    => $user,
                    'campaigns'  => $campaigns
                ];
                return $responseData;
            } else {
                throw new NotFoundHttpException("Object not found: $id");
            }
        }

        public function actionCreateuser(){
            $model = new User();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            //  $model->load(Yii::$app->request->post());
            $model->status          = User::STATUS_ACTIVE;
            $model->confirmed_at    = date('Y-m-d H:i:s');
            $model->campaign_id     = Yii::$app->request->post('campaign_id');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $sql = "INSERT INTO auth_item(name, type, description) VALUES ('$model->username',1,'$model->username')";
                $query = Yii::$app->db->createCommand($sql)->execute();
                $response->setStatusCode(201);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionUpdateUser($id) {
            $model = User::findOne($id);
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->campaign_id     = Yii::$app->request->post('campaign_id');
            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }
            return $model;
        }

        public function actionTldashboard($campaign_id,$startdate,$enddate){
            if(isset($campaign_id) && !empty($campaign_id)){
                $model              = new CustomerData();
                $totalrecords       = $model->getTotalRecords($campaign_id,$startdate,$enddate);
                $unassignedrecords  = $model->getUnAssignedRecords($campaign_id,$startdate,$enddate);
                $pendingrecords     = $model->getAssignedPendingRecords($campaign_id,$startdate,$enddate);
                $processedrecords   = $model->getAssignedProcessedRecords($campaign_id,$startdate,$enddate);
                $rejectedrecords    = $model->getRejectedRecords($campaign_id,$startdate,$enddate);
                $responseData = [
                    'totalrecords'                => $totalrecords,
                    'unassignedrecords'           => $unassignedrecords,
                    'pendingrecords'              => $pendingrecords,
                    'processedrecords'            => $processedrecords,
                    'rejectedrecords'             => $rejectedrecords,

                ];
                return $responseData;
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionGetassignedlist($campaign_id,$startdate,$enddate,$flag){
            if(isset($campaign_id) && !empty($campaign_id)){
                $campaigns = Campaign::findOne($campaign_id);
                if(isset($flag) && !empty($flag)){
                    $model  = new CustomerData();
                    if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
                        $date_range = $startdate.'|'.$enddate ;
                    }
                    else{
                        $date_range = '';
                    }
                    $data   = $model->getAssignedList($campaign_id,$startdate,$enddate,$flag);
                    $responseData = [
                        'assignedlist'    => $data,
                        'campaign_name'   => $campaigns->title,
                        'date_range'      => $date_range
                    ];
                    return $responseData;
                }
                else{
                    throw new HttpException(422, json_encode("Permission denied."));
                }
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionBulkassignlist($campaign_id,$startdate,$enddate){
            if(isset($campaign_id) && !empty($campaign_id)) {
                $campaigns = Campaign::findOne($campaign_id);
                if(isset($campaigns))
                {
                    $model = new CustomerData();
                    if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
                        $date_range = $startdate.'|'.$enddate ;
                    }
                    else{
                        $date_range = '';
                    }
                    $bulklistdetails    = $model->getBulkAssignDetailsList($campaign_id,$startdate,$enddate);
                    $unassignedrecords  = $model->getUnAssignedRecords($campaign_id,$startdate,$enddate);
                    $responseData = [
                        'bulklistdetails'    => $bulklistdetails,
                        'unassignedrecords'  => $unassignedrecords,
                        'campaign_name'      => $campaigns->title,
                        'date_range'         => $date_range
                    ];
                    return $responseData;
                }
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }


        public function actionBulkassigntoagent(){
            if(Yii::$app->request->post('campaign_id')){
                $campaign_id    = Yii::$app->request->post('campaign_id');
                $campaigns      = Campaign::findOne($campaign_id);
                if(isset($campaigns)) {
                    if (Yii::$app->request->post('bulkassignvalue')) {
                        $bulkassignvalue = Yii::$app->request->post('bulkassignvalue');
                        $startdate       = Yii::$app->request->post('startdate');
                        $enddate         = Yii::$app->request->post('enddate');
                        $decode          = Json::decode($bulkassignvalue);
                        $model           = new CustomerData();
                        $connection         =  \Yii::$app->db;
                        $transaction        = $connection->beginTransaction();
                        try {
                            foreach($decode as $key=>$value){
                                $sql = " INSERT INTO customer_assigned_users(customer_data_id, allocated_user_id, created_date, updated_date)
                                     SELECT id,".$key.",now(),now() 
                                     FROM customer_data 
                                     WHERE campaign_id = ".$campaign_id." 
                                          AND internal_status = 0
                                          AND status = 1 ";
                                          if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate))
                                          {
                                              $sql .=  " AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."' ";
                                          }
                                $sql .=  " ORDER BY created_on
                                    LIMIT ".$value." ";
                                $connection->createCommand($sql)->execute();
                                $upadtesql = " UPDATE customer_data 
                                                 SET internal_status = 1,
                                                     updated_date = now()
                                           FROM
                                           (
                                            SELECT id,campaign_id
                                            FROM customer_data 
                                            WHERE campaign_id = ".$campaign_id." 
                                                  AND internal_status = 0
                                                  AND status = 1 ";
                                                    if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate))
                                                    {
                                                        $upadtesql .=  " AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."' ";
                                                    }
                                        $upadtesql .=  " ORDER BY created_on
                                            LIMIT ".$value."
                                           ) AS ExistingCustdata 
                                            WHERE ExistingCustdata.id=customer_data.id
                                                  AND ExistingCustdata.campaign_id=customer_data.campaign_id ";
                                $connection->createCommand($upadtesql)->execute();
                            }
                            $transaction->commit();
                            if ($transaction) {
                                $response = \Yii::$app->getResponse();
                                $response->setStatusCode(200);
                                $bulklistdetails    = $model->getBulkAssignDetailsList($campaign_id,$startdate,$enddate);
                                $unassignedrecords  = $model->getUnAssignedRecords($campaign_id,$startdate,$enddate);
                                $responseData = [
                                    'bulklistdetails'    => $bulklistdetails,
                                    'unassignedrecords'  => $unassignedrecords,
                                ];
                                return $responseData;
                            } else {
                                throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                            }
                        }
                        catch(Exception $e) {
                            $transaction->rollback();
                        }
                    } else {
                        throw new HttpException(422, json_encode("Atleast assign a single record!!"));
                    }
                }
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionAgentlist(){
            $users=User::find()->where(['=', 'status', User::STATUS_ACTIVE])->andWhere(['=', 'role', User::ROLE_USER]) ->andWhere(['not', ['confirmed_at' => null]])->orderBy(['id'=>SORT_DESC])->all();
            return $users;
        }

        public function actionReassignlist($campaign_id,$startdate,$enddate,$user_id){
            if(isset($campaign_id) && !empty($campaign_id)){
                $model          = new CustomerData();
                $data     = $model->getReassignList($campaign_id,$startdate,$enddate,$user_id);
                 $responseData = [
                    'reassignlist'  => $data,
                 ];
                 return $responseData;
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionReassigntoagent(){
            if(Yii::$app->request->post('user_id')){
                if(Yii::$app->request->post('customer_data_id')){
                    $user_id           = Yii::$app->request->post('user_id');
                    $customer_data_id  = Yii::$app->request->post('customer_data_id');
                    $sql            = " UPDATE customer_assigned_users SET allocated_user_id = ".$user_id." WHERE customer_data_id in(".$customer_data_id.") ";
                    $query          = Yii::$app->db->createCommand($sql)->execute();
                    if($query){
                        $response = \Yii::$app->getResponse();
                        $response->setStatusCode(200);
                    }
                    else{
                        throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                    }
                }
                else{
                    throw new HttpException(422, json_encode("Atleast select one record!!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Please choose User!!"));
            }
        }

        public function actionAgentcampaignlist(){
            $user = User::findIdentity(\Yii::$app->user->getId());
            $campaignmodel  = new Campaign();
            $campaign_id    = $campaignmodel->getAgentCampaignId($user->id);
            $campaignid     = explode(',', $campaign_id);
            $campaigns      = Campaign::find()->where(['status'=>Campaign::STATUS_ACTIVE])->andWhere(['IN','id',$campaignid])->orderBy(['id'=>SORT_ASC])->all();
            return $campaigns;
        }

        public function actionAgentdatalist($campaign_id,$startdate,$enddate){
            if(isset($campaign_id) && !empty($campaign_id)){
                $user = User::findIdentity(\Yii::$app->user->getId());
                $model  = new CustomerData();
                $data   = $model->getAgentAssignedList($campaign_id,$startdate,$enddate,$user->id);
                $responseData = [
                     'agentdatalist'  => $data,
                ];
                return $responseData;
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionGetcustomerdata($campaign_id,$customer_data_id){
            if(isset($campaign_id) && !empty($campaign_id)){
                $campaigns  = Campaign::findOne($campaign_id);
                if(isset($customer_data_id) && !empty($customer_data_id)){
                    $model  = new CustomerData();
                    $data   = $model->getCustomerData($campaign_id,$customer_data_id);
                    $responseData = [
                        'customerdata'  => $data,
                        'campaign_name' => $campaigns->title,
                    ];
                    return $responseData;
                }
                else{
                    throw new HttpException(422, json_encode("Customer Data should not be empty!!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionAgentdataverify(){
            if(Yii::$app->request->post('campaign_id') && Yii::$app->request->post('user_id') && Yii::$app->request->post('customer_data_id')){
                $campaign_id       = Yii::$app->request->post('campaign_id');
                $user_id           = Yii::$app->request->post('user_id');
                $customer_data_id  = Yii::$app->request->post('customer_data_id');
                if(Yii::$app->request->post('status')) {
                    if(Yii::$app->request->post('agent_comments')) {
                        $status             = Yii::$app->request->post('status');
                        $agent_comments     = Yii::$app->request->post('agent_comments');
                        $sql   = "UPDATE customer_data SET internal_status = ".$status.", agent_comments = '".$agent_comments."',agent_comments_date = now()  WHERE id = ".$customer_data_id." AND campaign_id = ".$campaign_id;
                        $query = Yii::$app->db->createCommand($sql)->execute();
                        if($query){
                            $sql1   = "UPDATE customer_assigned_users SET status = 1, updated_date = now()  WHERE customer_data_id = ".$customer_data_id." AND allocated_user_id = ".$user_id;
                            $query1 = Yii::$app->db->createCommand($sql1)->execute();
                            if($query1){
                                $response = \Yii::$app->getResponse();
                                $response->setStatusCode(200);
                            }
                            else{
                                throw new HttpException(422, json_encode('Server error! Please try after some time!!'));
                            }
                        }
                        else{
                            throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                        }
                    }
                    else{
                        throw new HttpException(422, json_encode("Agent comments should not be empty!!"));
                    }
                }
                else{
                    throw new HttpException(422, json_encode("Please Select Status!!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Permission denied."));
            }
        }

    }