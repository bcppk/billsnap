<?php

namespace app\helpers;

use Yii;
use app\helpers\CromaOcrHelper;
use app\helpers\BigBazzarOcrHelper;

class OcrImagetoTextHelper {
    private $error;
    private $text;
    private $arrayType;

    function __constructor(){
        $this->error 	= false;
    }

    public function dynamsoftOcr($img){
        // construct post data
        /*$fileName = 'esempio.jpg';

        $filePath = dirname(__FILE__).DIRECTORY_SEPARATOR.$fileName;
        $args['file'] = new CurlFile($filePath, 'image/jpg', $fileName);*/
        $fileName = '5a3fc1031f206.jpeg';
        $filepath= 'E:/xampp/htdocs/ocrapi/api/uploads/bills/'.$fileName;
        $args['file'] = new CurlFile($filepath, 'image/jpg', $fileName);


        // upload file to DDC Rest
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://cloud.dynamsoft.com/rest/ocr/v1/file?method=upload&ondup=overwrite');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('x-api-key:sT0kItPqksIVS5XlaF/XcezBrJ+KjW3A0l6OkUhPIkH+PieFjox65w==', 'Content-Type: multipart/form-data'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // On dev server only
        echo curl_exec($ch);
        curl_close ($ch);
        exit;
        /*   echo "<pre/>";
          print_r($json_response);exit; */


    }

    public function googleOcr($img){

        $api_key = 'AIzaSyD-U3o9uNFlk_kaF8QWHrn2oOeKrSS4HMw';
        $cvurl = "https://vision.googleapis.com/v1/images:annotate?key=" . $api_key;
        $type = "TEXT_DETECTION";

        $data = file_get_contents($img);
        $base64 = base64_encode($data);


        $r_json ='{
			"requests": [
				{
				  "image": {
					"content":"' . $base64. '"
				  },
				  "features": [
					  {
						"type": "' .$type. '",
						"maxResults": 200
					   }
				  ]
				}
			]
		}';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $cvurl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $r_json);
        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ( $status != 200 ) {
            die("Error: $cvurl failed status $status" );
        }


        $data = json_decode($json_response);
        if(isset($data->responses[0]->textAnnotations[0]->description) && !empty($data->responses[0]->textAnnotations[0]->description)){
            $this->text 	 = $data->responses[0]->textAnnotations[0]->description;
            $this->arrayType = preg_split("/((?<!\\\|\r)\n)|((?<!\\\)\r\n)/", $data->responses[0]->textAnnotations[0]->description);
        }else{
            $this->error = "No Text";
        }
    }

    public function getText(){
        return $this->text;
    }

    public function isError(){
        if($this->error){
            return true;
        }
        return false;
    }

    public function getError(){
        return $this->error;
    }

    public function getarrayType(){
        return $this->arrayType;
    }

    public function getData($img)
    {
        $this->googleOcr($img);
        $text=$this->getText();
        $data=[];
        /*echo "<pre/>";
        print_r($text);exit;*/
        if($text)
        {
            $string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $text);

            if(preg_match('/reliance/', strtolower($string), $matches)){
                $relianceOcr = new RelianceOcrHelper();
                $data = $relianceOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/croma/', strtolower($string), $matches)){
                $cromaOcr = new CromaOcrHelper();
                $data = $cromaOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/crona/', strtolower($string), $matches)){
                $cromaOcr = new CromaOcrHelper();
                $data = $cromaOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/croha/', strtolower($string), $matches)){
                $cromaOcr = new CromaOcrHelper();
                $data = $cromaOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/infiniti retail/', strtolower($string), $matches)){
                $cromaOcr = new CromaOcrHelper();
                $data = $cromaOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/Infinits/', strtolower($string), $matches)){
                $cromaOcr = new CromaOcrHelper();
                $data = $cromaOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/infinit/', strtolower($string), $matches)){
                $cromaOcr = new CromaOcrHelper();
                $data = $cromaOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/CRONA/', strtolower($string), $matches)){
                $cromaOcr = new CromaOcrHelper();
                $data = $cromaOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/BIG BAZAAR/', strtoupper($string), $matches)){
                $bigbazzarOcr = new BigBazzarOcrHelper();
                $data = $bigbazzarOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/BIG/', strtoupper($string), $matches)){
                $bigbazzarOcr = new BigBazzarOcrHelper();
                $data = $bigbazzarOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/IG BAZAAR/', strtoupper($string), $matches)){
                $bigbazzarOcr = new BigBazzarOcrHelper();
                $data = $bigbazzarOcr->convertType($text);
                return $data;
            }
            else if(preg_match('/BLG BAZAAR/', strtoupper($string), $matches)){
                $bigbazzarOcr = new BigBazzarOcrHelper();
                $data = $bigbazzarOcr->convertType($text);
                return $data;
            }
            else{
                $val['storeType']	 = 'no_header';
                return $val;
            }
        }
        else
        {
            $val['storeType']		 = 'invalid_image';
            return $val;
        }

    }


}

    
 
?>