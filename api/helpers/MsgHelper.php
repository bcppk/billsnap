<?php
namespace app\helpers;

use Yii;


class MsgHelper
{
    public function sendMail($sub, $msg, $to)
    {
        $url = 'https://api.sendgrid.com/';
        $user = 'integlo';
        $pass = 'integlo2@15';

        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => $to,
            'subject'   => $sub,
            'html'      => $msg,
            'text'      => $msg,
            'from'      => 'no-reply@bigcityexperience.com',
        );


        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        //curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);

        // obtain response
        $response = curl_exec($session);
        curl_close($session);

        // print everything out
        $result = json_decode($response,true);
        //return $response;
        return $result['message'] === 'success';
    }

    public function sendSMS($to, $msg = null)
    {
        //echo 'mobile no to send sms: ' . $m;
        //http://instant.sinfini.com/web2sms.php?
        if (!isset($msg)) $msg = 'This is a BigCity confirmation sms';
        //$url = 'http://alerts.sinfini.com/api/v3/index.php?method=sms&api_key=A52152bf0a40b7434a3bad1d59d7864b4&to=' . $m . '&sender=BGCITY&message=' . $msg . '&format=json&custom=1,2&flash=0';
        //$url = 'http://alerts.sinfini.com/api/v3/index.php?';
        $url = 'http://alerts.sinfini.com/api/v3/index.php?method=sms&api_key=A52152bf0a40b7434a3bad1d59d7864b4&to=' . $to . '&sender=BIGCTY&message=' . $msg . '&format=json&custom=1,2&flash=0';
        //echo '<br/>' . $url;

        /*$response = Yii::$app->httpclient->request($url,'GET', null, ['query' => [
            'method' => 'sms',
            'api_key' => 'A52152bf0a40b7434a3bad1d59d7864b4',
            'to' => $to,
            'sender' => 'BIGCTY',
            'message' => $msg,
            'format' => 'json',
            'custom' => '1,2',
            'flash' => 0
        ]
        ]);*/

        $response = $this->httpGet($url);
        return $response;
        //return $response['status'] === "OK";
    }

    public function httpGet($url)
    {
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //  curl_setopt($ch,CURLOPT_HEADER, false);

        $output=curl_exec($ch);

        curl_close($ch);
        return $output;
    }

}