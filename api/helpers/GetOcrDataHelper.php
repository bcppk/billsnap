<?php
namespace app\helpers;

use Yii;
use app\helpers\OcrImagetoTextHelper;

class GetOcrDataHelper
{
    function ocrStep1($billsanp_id)
    {
        $query = "SELECT Billsnap.id AS billsnap_id ,
						 Billsnap.bill_image AS image,
						 Billsnap.status
				FROM billsnap AS Billsnap
				WHERE Billsnap.id = ".$billsanp_id." 
				      AND (Billsnap.status = 0 OR Billsnap.status = 102)
				ORDER BY Billsnap.id";

        $result = Yii::$app->db->createCommand($query)->queryAll();
        if(isset($result) && !empty($result))
        {
            foreach($result as $value)
            {
                $img   				= $value['image'];
                $file_size			= $this->getFileSize($img);
                if($file_size > 4024000 )
                {
                    $status_update = "UPDATE billsnap SET status=100,billsnap_comments='Maximum filesize accepted is 4 MB',updated_date=now() WHERE id =".$value['billsnap_id'];
                    $query = Yii::$app->db->createCommand($status_update)->execute();
                }
                else
                {
                    $response			= new OcrImagetoTextHelper();
                    $data		 		= $response->getData($img);
                     /*echo "<pre/>";
                    print_r($data);
                    exit;*/
                    if(isset($data['storeType']))
                    {
                        if($data['storeType']=='invalid_image')
                        {
                            $status_update = "UPDATE billsnap SET status=101,billsnap_comments='We are sorry, the bill you have uploaded not a valid bill.',updated_date=now() WHERE id =".$value['billsnap_id'];
                            $query = Yii::$app->db->createCommand($status_update)->execute();
                        }
                        else if($data['storeType']=='no_header')
                        {
                            $status_update 			= "UPDATE billsnap SET status=102,billsnap_comments='The bill you have uploaded is not readable. Please resubmit a clear bill.',updated_date=now() WHERE id =".$value['billsnap_id'];
                            $query = Yii::$app->db->createCommand($status_update)->execute();
                        }
                        else if($value['status']== 102 && (!empty($data['itemdetails']) && ( !empty($data['full_invoiceno']) || !empty($data['invoiceno']) || !empty($data['date']) )) ){
                            $status_update 			= "UPDATE billsnap SET status=104,billsnap_comments='Thank you for submission. We will get back to you in 24 hrs.',reason='Image quality bad.',updated_date=now() WHERE id =".$value['billsnap_id'];
                            $query = Yii::$app->db->createCommand($status_update)->execute();
                        }
                        else
                        {
                            $jsondata			= json_encode($data);
                            $update_sql  	    = "UPDATE billsnap SET billsnap_data = '".$jsondata."',status=5,billsnap_comments = 'OCR In-Progress' ,updated_date=now(),store_id = ".$data['store_id']." WHERE id = ".$value['billsnap_id'];
                            $query = Yii::$app->db->createCommand($update_sql)->execute();

                        }
                    }
                }
            }
        }
        return 1;
    }



    function getFileSize($img){
        $ch = curl_init($img);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);
        $data = curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

        curl_close($ch);
        return $size;
    }

    function verifyOcrdataByProduct($billsnap_id,$store_id){
        $sql = "SELECT StoreProd.product_name,
                         StoreProd.mrp_price
                FROM store_products AS StoreProd
                JOIN (
                SELECT TRIM(ProdNamewithtrim.prod_name1) AS prod_name1,
                       TRIM(ProdNamewithtrim.prod_name2) AS prod_name2,
                       SUBSTRING(RTRIM(ProdNamewithtrim.prod_name2),2,length(RTRIM(ProdNamewithtrim.prod_name2))-3) AS prod_name3,
                       SUBSTRING(RTRIM(SUBSTRING(RTRIM(ProdNamewithtrim.prod_name2),2,length(RTRIM(ProdNamewithtrim.prod_name2))-3)),2,length(RTRIM(SUBSTRING(RTRIM(ProdNamewithtrim.prod_name2),2,length(RTRIM(ProdNamewithtrim.prod_name2))-3)))-3) AS prod_name4
                FROM
                (
                SELECT SUBSTRING(prod_name,2,length(prod_name)-1) AS prod_name1,
                       SUBSTRING(prod_name,2,length(prod_name)-2) AS prod_name2
                FROM
                (
                    SELECT TRIM(REPLACE(REPLACE(REPLACE(unnest(string_to_array(Billsnap.billsnap_data->>'itemdetails', ',')),'\"',''),'[',''),']','')) AS prod_name
                    FROM billsnap AS Billsnap
                    WHERE id = ".$billsnap_id." 
                          AND Billsnap.status=1 
                          AND Billsnap.store_verify = 0
                )AS Prodname
                ) AS ProdNamewithtrim
                ) AS BillsnapItemDetails ON (StoreProd.product_name ilike '%'||TRIM(BillsnapItemDetails.prod_name1)||'%' OR StoreProd.product_name = TRIM(BillsnapItemDetails.prod_name1) OR StoreProd.product_name ilike '%'||TRIM(BillsnapItemDetails.prod_name2)||'%' OR StoreProd.product_name = TRIM(BillsnapItemDetails.prod_name2) OR StoreProd.product_name ilike '%'||TRIM(BillsnapItemDetails.prod_name3)||'%' OR StoreProd.product_name = TRIM(BillsnapItemDetails.prod_name3) OR StoreProd.product_name ilike '%'||TRIM(BillsnapItemDetails.prod_name4)||'%' OR StoreProd.product_name = TRIM(BillsnapItemDetails.prod_name4))   
                WHERE StoreProd.store_id = ".$store_id." ";

        $data = Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]) && !empty($data[0]))
            return $data[0];
        else
            return 0;
    }

    function checkOcrResults()
    {
        $sql  = "SELECT * FROM billsnap_data";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]) && !empty($data[0]))
        {
            return $data[0];
        }
        else
        {
            return false;
        }
    }

    function ocrStep2($billsnap_id)
    {
        $check_ocr_results	= $this->checkOcrResults();
        if($check_ocr_results)
        {
            $this->checkDuplicateOcrData($check_ocr_results['all_bill_data'],$billsnap_id);
        }
        else
        {
            $sql 	= "SELECT * FROM billsnap WHERE id = ".$billsnap_id." AND status=5 ";
            $data   = Yii::$app->db->createCommand($sql)->queryAll();
            if(isset($data[0]) && !empty($data[0]))
            {
                $value		  = $data[0];
                $array_data	  = json_decode($value['billsnap_data'], true);
                $billdata[]	  = $array_data;
                $jsonData	  = json_encode($billdata);
                $insert_query = "INSERT INTO billsnap_data(all_bill_data) VALUES ('".$jsonData."');";
                $results	  = Yii::$app->db->createCommand($insert_query)->execute();
                if($results)
                {
                    $updated_query		= "UPDATE billsnap SET status=1,billsnap_comments='OCR Processed',updated_date=now() WHERE id = ".$value['id'];
                    $query	            = Yii::$app->db->createCommand($updated_query)->execute();
                }
                else
                {
                    return false;
                }
            }
        }
        return 1;
    }

    function checkDuplicateOcrData($ocr_existing_json,$billsnap_id)
    {
        $query ="SELECT Final.*
                 FROM
                 (
                 SELECT OcrTemp.id,
                        3 AS status,
                        '{}'::json as json_data
                 FROM
                 (
                 SELECT DISTINCT OcrRes.res_data->>'customername' as customername,
                        OcrRes.res_data->>'customer_mobileno' as customer_mobileno,
                        TRIM(OcrRes.res_data->>'invoiceno') as invoiceno,
                        TRIM(OcrRes.res_data->>'full_invoiceno') as full_invoiceno,
                        TRIM(OcrRes.res_data->>'date') as date,
                        OcrRes.temp_data->>'customername' as temp_customername
                 FROM
                 (
                 SELECT json_array_elements(billsnap_data.all_bill_data) AS res_data,billsnap.billsnap_data as temp_data
                 FROM billsnap_data,billsnap 
                 WHERE billsnap.id = ".$billsnap_id."
                       AND billsnap.status = 5
                 ) AS OcrRes
                 ) AS OcrResult
                 LEFT JOIN billsnap AS OcrTemp ON (TRIM(OcrTemp.billsnap_data->>'date')=OcrResult.date  AND TRIM(OcrTemp.billsnap_data->>'invoiceno')=OcrResult.invoiceno OR TRIM(OcrTemp.billsnap_data->>'full_invoiceno')=OcrResult.full_invoiceno OR  OcrTemp.billsnap_data->>'customer_mobileno'=OcrResult.customer_mobileno OR OcrTemp.billsnap_data->>'customername'=OcrResult.customername )
                 WHERE  OcrTemp.id = ".$billsnap_id." 
                        AND OcrTemp.status = 5
                 UNION ALL
                 SELECT id,
                     1 AS status,
                     billsnap_data::json AS json_data 
                  FROM billsnap
                  WHERE id = ".$billsnap_id."
                        AND status = 5
                 ) AS Final
                 ORDER BY Final.status DESC
                 LIMIT 1";
        $result = Yii::$app->db->createCommand($query)->queryAll();
        $existing_json_str = json_decode($ocr_existing_json, true);
        $data=$existing_json_str;
        if(isset($result) && !empty($result))
        {
            foreach($result as $value)
            {
                if($value['status']==1)
                {
                    $json_str = json_decode($value['json_data'],true);
                    array_push($data,$json_str);

                    $updated_ocr_query  = "UPDATE billsnap SET status=1,billsnap_comments='OCR Processed',updated_date=now() WHERE id = ".$value['id'];
                    $query	            = Yii::$app->db->createCommand($updated_ocr_query)->execute();
                }
                else if($value['status']==3)
                {
                    $update_ocr_query	 = "UPDATE billsnap SET status=106,billsnap_comments='You have already submitted this bill.',updated_date=now() WHERE id = ".$value['id'];
                    $query	             = Yii::$app->db->createCommand($update_ocr_query)->execute();
                }
            }
            $jsonData = json_encode($data);
            $update_ocr_query 	 = "UPDATE billsnap_data SET all_bill_data='".$jsonData."' ";
            $query	             = Yii::$app->db->createCommand($update_ocr_query)->execute();
        }

    }

}