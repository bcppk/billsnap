<?php
namespace app\models;

use Yii;
use app\models\Reward;
use app\models\Campaign;

/**
 * This is the model class for table "reward_messages".
 *
 * @property integer $id
 * @property integer $campaign_id
 * @property integer $reward_id
 * @property string $messages
 * @property integer $status
 * @property timestamp $created_date
 * @property timestamp $updated_date
 */
class RewardMessage extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reward_messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_id','reward_id','messages'], 'required'],
            [['id','reward_id'], 'integer'],
            [['messages'], 'string'],
            [['messages'], 'string', 'max' => 500],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reward_id' => 'Reward ID',
            'messages' => 'Messages',
            'status' => 'Status',
        ];
    }

    public function getReward()
    {
        return $this->hasOne(Reward::className(), ['id' => 'reward_id']);
    }

    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    public function fields()
    {
        return [
            'id',
            'product' => function($model) {
                return $model->reward->prod_name;
            },
            'reward_id' => function($model) {
                return $model->reward_id;
            },
            'campaign_id' => function($model) {
                return $model->campaign_id;
            },
            'messages' => function($model) {
                return $model->messages;
            },
            'status' => function($model) {
                return $model->status;
            },
            'campaign_title' => function($model) {
                return $model->campaign->title;
            },
        ];
    }



    public function getRewardMessageByCampaignandReward($campaign_id,$reward_id)
    {
         $sql = "SELECT messages 
                 FROM reward_messages AS RewardMessage 
                 WHERE RewardMessage.status = 1
                       AND RewardMessage.reward_id = ".$reward_id."
                       AND RewardMessage.campaign_id = ".$campaign_id." ";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]) && !empty($data[0]))
            return $data[0]['messages'];
        else
            return 0;
    }

}
