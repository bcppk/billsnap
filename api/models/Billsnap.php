<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property json $billsnap_data
 * @property string $billsnap_comments
 * @property string $bill_image
 * @property integer $status
 * @property integer $store_verify
 * @property integer $store_id
 * @property timestamp $created_date
 * @property timestamp $updated_date
 * @property integer $customer_data_id
 */
class Billsnap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'billsnap';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_image'], 'required'],
            [['created_date','updated_date'], 'safe'],
            [['store_verify','store_id','customer_data_id'], 'integer'],
            [['billsnap_data','billsnap_comments','bill_image'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bill_image' => 'Bill Image',
            'created_date' => 'Createdon',
        ];
    }



}
