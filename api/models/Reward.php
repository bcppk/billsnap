<?php
namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "vouchers".
 *
 * @property integer $id
 * @property string $code
 * @property integer $status
 * @property string $authCode
 * @property string $regTill
 * @property string $bookTill
 */
class Reward extends \yii\db\ActiveRecord
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rewards';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod_name', 'value', 'status'], 'required'],
            [['status'], 'integer'],
            [['prod_name'], 'string', 'max' => 100],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' =>  TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('Y-m-d H:i:s')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_name' => 'Product Name',
            'value' => 'Reward Value',
            'status' => 'Status',
        ];
    }

    public function getRewardsbyCampaign($campaign_id){
        $sql = "SELECT DISTINCT Reward.*
                FROM rewards AS Reward
                    -- JOIN reward_messages AS RwdMsg ON (RwdMsg.reward_id=Reward.id)
                WHERE Reward.status=1 ";
                     // AND RwdMsg.campaign_id=".$campaign_id
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data) && !empty($data))
            return $data;
        else
            return 0;
    }
}
