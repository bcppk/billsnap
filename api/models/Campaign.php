<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "campaigns".
 *
 * @property integer $id
 * @property string $analyticsCode
 * @property string $title
 * @property string $regText
 * @property string $contactNo
 * @property string $txtContact
 * @property string $txtTerms
 * @property string $postregmsgvalid
 * @property string $postregmsginvalid
 * @property timestamp $startDate
 * @property timestamp $endDate
 * @property string $disabledWeekDays
 * @property string $sitebanner
 * @property smallint $ocr_enable
 * @property integer $status
 * @property timestamp $created_date
 * @property timestamp $updated_date
 */
class Campaign extends \yii\db\ActiveRecord
{

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaigns';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'contactno'], 'required'],
            [['id'], 'integer'],
            [['regtext', 'txtcontact', 'txtterms'], 'string'],
            [['analyticscode'], 'string', 'max' => 5000],
            [['title'], 'string', 'max' => 100],
            [['contactno'], 'string', 'max' => 15],
            [['postregmsgvalid', 'postregmsginvalid'], 'string', 'max' => 1000],
            [['menubarcolor'], 'string', 'max' => 10],
            [['sitebanner'], 'string', 'max' => 50],
            [['startdate', 'enddate'], 'date', 'format' => 'yyyy-M-d H:m:s'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'analyticsCode' => 'Google Analytics Code',
            'title' => 'Campaign Title',
            'regText' => 'Registration OTP Text',
            'contactno' => 'Campaign Contact No',
            'txtContact' => 'Contact Us Page',
            'txtTerms' => 'Terms & Conditions Page',
            'postregmsgvalid' => 'After Registration Valid Message',
            'postregmsginvalid' => 'After Registration Invalid Message',
            'startdate' => 'Start Date for Preferred Dates',
            'enddate' => 'End Date for Preferred Dates',
            'disabledWeekDays' => 'Disabled Week Days (0-Sunday....7-Saturday. eg.: 0,7)',
            'sitebanner' => 'Website Banner',
            'ocr_enable' => 'OCR Enabled',
            'status' => 'Status',
        ];
    }

    public function DisabledWeekDays()
    {
        if (!isset($this->disabledWeekDays) or empty($this->disabledWeekDays))
            return [];
        return explode(',',$this->disabledWeekDays);
    }

    public function getSiteSettings()
    {
        return static::find()->one();
    }
    public function parseCampginSmsTemplate($msg,$arrParams)
    {
        if(isset($msg))
        {
            foreach($arrParams as $param)
            {
                $msg = str_ireplace( '{{' . $param["name"] . '}}', $param["value"], $msg);
            }
            $msg = trim(preg_replace('/\s\s+/', ' ', $msg));
            return $msg;
        }
        return '';
    }
    public function getAgentCampaignId($user_id,$booking_status,$bookassign_status){
        $sql = "SELECT array_to_string(array_agg(campaign_id),',') AS campaign_id
                FROM
                (
                    SELECT DISTINCT Booking.campaign_id
                    FROM booking AS Booking 
                         JOIN booking_assigned_users AS BAU ON (BAU.booking_id=Booking.id AND BAU.allocated_user_id=".$user_id." AND BAU.status=".$bookassign_status.")
                    WHERE Booking.status=".$booking_status."
                order by 1
                ) AS A";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]['campaign_id']) && !empty($data[0]['campaign_id']))
            return $data[0]['campaign_id'];
        else
            return 0;

    }
}
